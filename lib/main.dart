import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  //Change the counter
  void _changeCounter() {
    setState(() {
      counter += 1;
      if(counter > 4) {
        _isFull = true ;
      }
    });
  }

  //Change the Icon details
  void _changeIcon() {
    setState(() {
      if(_isFull) {
        _iconColor = Colors.lightGreen ;
        _buttonColor = Colors.amber[_iconGradient + 100];
        refillText = 'FULL';
      } else {
        _iconColor = Colors.blue;
        _buttonColor = Colors.blue ;
      }
      _iconsize += 50.0;
      _iconGradient += 100;
      _backColor = Colors.amber[_iconGradient];
      _sizeOfBox -= 50;
    });
  }

  //State variables
  int counter = 0;
  Color _iconColor = Colors.red;
  Color _buttonColor = Colors.red;
  double _iconsize = 50.0;
  Color _backColor = Colors.white;
  int _iconGradient = 100;
  double _sizeOfBox = 300;
  String refillText = 'REFILL' ;
  bool _isFull = false;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        home: Scaffold(
            backgroundColor: _backColor,
            appBar: AppBar(
              title: Text('Play with Me'),
              centerTitle: true,
              backgroundColor: Colors.amber,
            ),
            body: Center(
              child: Column(
                children: [
                  SizedBox(
                    height: _sizeOfBox,
                  ),
                  Container(
                    child: IconButton(
                      icon: ( _buildIcon(counter) ),
                      color: _iconColor,
                      iconSize: _iconsize,
                      onPressed: () {},
                    ),
                  ),

                  FlatButton(
                    onPressed: () {
                      if(_isFull) {}
                      else {
                        _changeCounter();
                        _changeIcon();
                      }
                    },
                    color: _buttonColor,
                    textColor: Colors.white,

                    child: Text(
                      '$refillText',
                      style: TextStyle(fontSize: 20),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    '$counter',
                    style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            )
        )
    );
  }
}

//Build an Icon based on Counter
Widget _buildIcon(int count) {
  if(count == 0) {
  return new Icon(
    Icons.battery_alert,
  );}
  else if (count < 5) {
    return new Icon(
      Icons.battery_charging_full,
    );
  }
  else {
    return new Icon(
      Icons.battery_full,
    );
  }
}
